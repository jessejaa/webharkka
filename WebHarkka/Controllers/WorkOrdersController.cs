﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebHarkka.Data;
using WebHarkka.Models;
using WebHarkka.ViewModels;

namespace WebHarkka.Controllers
{

    /**
     * TODO:
     * - (kaikki) Kommenttien lisääminen details näkymään
     * - (asiakas) Tilauksen muuttaminen vain jos ORDERED
     * - (asiakas) Tilauksen poistaminen vain jos ORDERED tähän dialog
     */

    [Authorize]
    public class WorkOrdersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private string userId;
        private readonly UserManager<AppUser> _userManager;
        private ClaimsPrincipal user;

        public WorkOrdersController(ApplicationDbContext context, UserManager<AppUser> userManager, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            var httpUser = httpContextAccessor.HttpContext.User;
            user = httpUser;
            userId = userManager.GetUserId(httpUser);
            this._userManager = userManager;
            user.IsInRole("Manager");
        }

        [HttpGet]
        public async Task<IActionResult> ModalAddMaterial(int order)
        {
            // Haetaan aikaisemmat kustanteet
            var workOrders = await _context.WorkOrders.Include(w => w.Materials).ToListAsync();

            List<Material> materials = workOrders.Find(x => x.Id == order).Materials;
            
            return PartialView(new MaterialsViewModel() { Materials = materials, NewMaterial = new Material() });
        }

        [HttpPost]
        public async Task<IActionResult> AddMaterial(int order, Material material)
        {
            if (ModelState.IsValid)
            {
                var workOrders = await _context.WorkOrders.Include(w => w.Materials).ToListAsync();

                WorkOrder o = workOrders.Find(x => x.Id == order);

                o.Materials.Add(material);

                _context.Update(o);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));

            }

            return View(nameof(Index));
        }
        
        public async Task<IActionResult> OrdersTable(string search, string date, int state)
        {
            List<WorkOrder> temp = await _context.WorkOrders.Include(w => w.User).ToListAsync();
            
            if (User.IsInRole("Admin") || User.IsInRole("Manager"))
            {
                var s = search != null ? search : "";
                temp = temp.Where(o => o.User.Email.Contains(s)).ToList();
            }

            if (user.IsInRole("Customer"))
            {
                temp = temp.Where(o => o.UserId == userId).ToList();
            }

            if (date != "")
            {
                DateTime d = Convert.ToDateTime(date);
                temp = temp.Where(o => o.OrderDate.Date >= d.Date).ToList();
            }

            if (state != -1)
            {
                temp = temp.Where(o => (int)o.State == state).ToList();
            }
            

            return PartialView(nameof(OrdersTable), temp);
        }

        // Muuta tehtävän tila
        public IActionResult ChangeState(int orderId, int state)
        {

            return View(nameof(Index));
        }

        // Lisää kustannus tilaukselle
        //[Authorize(Roles = ("Manager"))]
        //public async Task<IActionResult> AddMaterial(int orderId, Material material)
        //{
        //    var order = _context.WorkOrders.Find(orderId);

        //    order.Materials.Add(material);

        //    await _context.SaveChangesAsync();

        //    return View(nameof(Index));
        //}

        // GET: WorkOrders
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _context.WorkOrders.Include(w => w.User);

            //List<WorkOrder> workOrders = new List<WorkOrder>();

            //if (user.IsInRole("Customer"))
            //{
            //    workOrders = await _context.WorkOrders.Where(w => w.User.Id == userId).ToListAsync();
            //}

            //if (user.IsInRole("Manager"))
            //{
            //    workOrders = await _context.WorkOrders.Include(w => w.User).ToListAsync();
            //}

            //if (user.IsInRole("Admin"))
            //{
            //    workOrders = await _context.WorkOrders.Include(w => w.User).ToListAsync();
            //}

            //return View(workOrders);

            return View();
        }

        // GET: WorkOrders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workOrder = await _context.WorkOrders
                .Include(w => w.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workOrder == null)
            {
                return NotFound();
            }

            return View(workOrder);
        }

        // GET: WorkOrders/ModalCreate
        public IActionResult ModalCreate()
        {
            // Syötetään uusi order objekti
            return PartialView(new WorkOrder());
        }

        // POST: WorkOrders/ModalCreate
        [HttpPost]
        public async Task<IActionResult> ModalCreate(WorkOrder workOrder)
        {
            if (ModelState.IsValid)
            {
                workOrder.UserId = userId;
                workOrder.OrderDate = DateTime.Now;
                workOrder.State = OrderState.Ordered;
                _context.Add(workOrder);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return PartialView(new WorkOrder());
        }

        // GET: WorkOrders/Create
        public IActionResult Create()
        {
            ViewData["UserIds"] = new SelectList(_context.Users, "Id", "Id");
            ViewData["UserId"] = userId;
            ViewData["DateNow"] = DateTime.Now.ToString("yyyy-MM-dd");
            return View();
        }

        // POST: WorkOrders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserId,Description,OrderDate,StartDate,DoneDate,ApproveDate,RejectDate,State")] WorkOrder workOrder)
        {
            if (ModelState.IsValid)
            {
                _context.Add(workOrder);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", workOrder.UserId);
            ViewData["UserId"] = userId;
            return View(workOrder);
        }

        // GET: WorkOrders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewData["UserIds"] = new SelectList(_context.Users, "Id", "Id");
            ViewData["UserId"] = userId;
            ViewData["DateNow"] = DateTime.Now.ToString("yyyy-MM-dd");

            var workOrder = await _context.WorkOrders.FindAsync(id);
            if (workOrder == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", workOrder.UserId);
            return View(workOrder);
        }

        // POST: WorkOrders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserId,Description,OrderDate,StartDate,DoneDate,ApproveDate,RejectDate,State")] WorkOrder workOrder)
        {
            if (id != workOrder.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(workOrder);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WorkOrderExists(workOrder.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", workOrder.UserId);
            return View(workOrder);
        }

        // GET: WorkOrders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workOrder = await _context.WorkOrders
                .Include(w => w.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workOrder == null)
            {
                return NotFound();
            }

            return View(workOrder);
        }

        // POST: WorkOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var workOrder = await _context.WorkOrders.FindAsync(id);
            _context.WorkOrders.Remove(workOrder);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WorkOrderExists(int id)
        {
            return _context.WorkOrders.Any(e => e.Id == id);
        }
    }
}
