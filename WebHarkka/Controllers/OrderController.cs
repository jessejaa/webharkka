﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebHarkka.Data;
using WebHarkka.Models;

namespace WebHarkka.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private ApplicationDbContext context;
        private readonly UserManager<AppUser> userManager;
        private AppUser appUser;
        private List<string> roles;

        public OrderController(ApplicationDbContext context, UserManager<AppUser> userManager, IHttpContextAccessor httpContextAccessor)
        {
            this.context = context;

            this.userManager = userManager;

            var httpUser = httpContextAccessor.HttpContext.User;
            
            var id = userManager.GetUserId(httpUser);

            appUser = context.Users.FirstOrDefault(x => x.Id == id);
            
            if (appUser.WorkOrders == null)
            {
                appUser.WorkOrders = new Collection<WorkOrder>();
                appUser.WorkOrders.Add(new WorkOrder() { Description = "teseti" });
                context.Users.Update(appUser);
                context.SaveChanges();
            }

            var vittu = "saatana";

        }

        // GET: Order
        public async Task<ActionResult> Index()
        {
            // Tämä siis lista omista tarjouksista
            var data = new List<WorkOrder>();

            if (await userManager.IsInRoleAsync(appUser, "Admin"))
            {
                data = context.WorkOrders.ToList();
            } else
            {
                data = context.WorkOrders.Where(x => x.UserId == appUser.Id).ToList();
            }
            return View(data);
        }

        // GET: Order/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Order/Create
        public ActionResult Create()
        {
            
            return View();
        }

        // POST: Order/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(WorkOrder order)
        {
            try
            {
                // TODO: Add insert logic here
                appUser.WorkOrders.Add(order);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Order/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Order/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here



                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Order/Delete/5
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Order/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}