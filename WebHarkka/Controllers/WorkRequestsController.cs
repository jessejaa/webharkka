﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebHarkka.Data;
using WebHarkka.Models;
using WebHarkka.ViewModels;

namespace WebHarkka.Controllers
{
    [Authorize]
    public class WorkRequestsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private string userId;
        private readonly UserManager<AppUser> _userManager;

        public WorkRequestsController(ApplicationDbContext context, UserManager<AppUser> userManager, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            var httpUser = httpContextAccessor.HttpContext.User;
            userId = userManager.GetUserId(httpUser);
        }

        public async Task<IActionResult> RequestsTable(string search, string date, int state)
        {
            List<WorkRequest> temp = await _context.WorkRequests.Include(w => w.User).ToListAsync();

            search = search != null ? search.ToLower() : "";

            DateTime d = Convert.ToDateTime(date);

            if (User.IsInRole("Admin") || User.IsInRole("Manager"))
            {
                temp = temp.Where(
                    o => o.RequestDate.Date >= d.Date
                    && o.Description.Contains(search)
                    || o.User.Name.ToLower().Contains(search)
                ).ToList();
            }

            if (User.IsInRole("Customer"))
            {
                temp = temp.Where(
                    o => o.UserId == userId
                        && o.Description.ToLower().Contains(search)
                        && o.RequestDate.Date >= d.Date
                ).ToList();

                if (state != -1)
                {
                    temp = temp.Where(o => (int)o.State == state).ToList();
                }

            }

            return PartialView(nameof(RequestsTable), temp);
        }

        // GET: WorkRequests/ModalCreate
        public IActionResult ModalCreate()
        {
            // Syötetään uusi order objekti
            return PartialView(new WorkRequest());
        }

        // POST: WorkOrders/ModalCreate
        [HttpPost]
        public async Task<IActionResult> ModalCreate(WorkRequest request)
        {
            if (ModelState.IsValid)
            {
                request.UserId = userId;
                request.RequestDate = DateTime.Now;
                request.State = RequestState.Requested;
                _context.Add(request);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return PartialView(new WorkOrder());
        }

        // GET
        public IActionResult ModalAddCostEstimate(int requestId)
        {
            return PartialView(new CostEstimateViewModel() { RequestId = requestId });
            //return PartialView(new CostEstimateViewModel());
        }

        // POST
        [HttpPost]
        public async Task<IActionResult> AddCostEstimate(CostEstimateViewModel cost)
        {
            if (ModelState.IsValid)
            {
                var applicationDbContext = _context.WorkRequests.Include(w => w.User);
                var t = await applicationDbContext.ToListAsync();

                WorkRequest r = t.Find(z => z.Id == cost.RequestId);
                r.CostEstimate = cost.CostEstimate;
                r.State = RequestState.Responded;

                _context.Update(r);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return PartialView(cost);
        }
        
        public async Task<IActionResult> ChangeState(int id, RequestState state)
        {
            var applicationDbContext = _context.WorkRequests.Include(w => w.User);
            var l = await applicationDbContext.ToListAsync();

            WorkRequest r = l.Find(x => x.Id == id);
            r.State = state;


            _context.Update(r);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }


        // GET: WorkRequests
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.WorkRequests.Include(w => w.User);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: WorkRequests/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workRequest = await _context.WorkRequests
                .Include(w => w.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workRequest == null)
            {
                return NotFound();
            }

            return View(workRequest);
        }

        // GET: WorkRequests/Create
        public IActionResult Create()
        {
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id");
            return View();
        }

        // POST: WorkRequests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserId,Description,RequestDate,State,CostEstimate")] WorkRequest workRequest)
        {
            if (ModelState.IsValid)
            {
                _context.Add(workRequest);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", workRequest.UserId);
            return View(workRequest);
        }

        // GET: WorkRequests/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workRequest = await _context.WorkRequests.FindAsync(id);
            if (workRequest == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", workRequest.UserId);
            return View(workRequest);
        }

        // POST: WorkRequests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserId,Description,RequestDate,State,CostEstimate")] WorkRequest workRequest)
        {
            if (id != workRequest.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(workRequest);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WorkRequestExists(workRequest.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", workRequest.UserId);
            return View(workRequest);
        }

        // GET: WorkRequests/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workRequest = await _context.WorkRequests
                .Include(w => w.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workRequest == null)
            {
                return NotFound();
            }

            return View(workRequest);
        }

        // POST: WorkRequests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var workRequest = await _context.WorkRequests.FindAsync(id);
            _context.WorkRequests.Remove(workRequest);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WorkRequestExists(int id)
        {
            return _context.WorkRequests.Any(e => e.Id == id);
        }
    }
}
