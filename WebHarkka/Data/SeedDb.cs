﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebHarkka.Models;

namespace WebHarkka.Data
{
    public class SeedDb
    {
        public static async Task CreatePropertyTypes(ApplicationDbContext dbContext)
        {

            List<PropertyType> propertyTypes = dbContext.PropertyTypes.ToList();

            string[] propertyTypeNames = {
                "Omakotitalo",
                "Vapaa-ajan asunto",
                "Maatila",
                "Kerrostalo",
                "Kerrostaloasunto",
                "Rivitalo",
                "Rivitaloasunto"
            };

            foreach (var name in propertyTypeNames)
            {
                var exists = propertyTypes.Exists(p => p.Name == name);

                if (!exists)
                {
                    PropertyType pt = new PropertyType
                    {
                        Name = name
                    };

                    await dbContext.PropertyTypes.AddAsync(pt);
                }

                dbContext.SaveChanges();
            }
        }

        public static async Task CreateRoles(IServiceProvider serviceProvider, IConfiguration Configuration, ApplicationDbContext dbContext)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<AppUser>>();

            string[] roleNames = {
                "Admin", // Admin
                "Manager", // Toimittaja
                "Customer" // Asiakas
            };

            IdentityResult roleResult;
            
            foreach (var roleName in roleNames)
            {
                // luodaan roolit ja lisätään kantaan
                var roleExist = await roleManager.RoleExistsAsync(roleName);

                if (!roleExist)
                {
                    roleResult = await roleManager.CreateAsync(new IdentityRole(roleName));
                }
            }

            // tehdään superuser käyttäjien hallintaa varten
            var poweruser = new AppUser
            {
                UserName = Configuration.GetSection("Admin")["Email"],
                Email = Configuration.GetSection("Admin")["Email"]
            };

            string userPassword = Configuration.GetSection("Admin")["Password"];

            var user = await userManager.FindByEmailAsync(Configuration.GetSection("Admin")["Email"]);

            if (user == null)
            {
                //await userManager.DeleteAsync(user);
                var createPowerUser = await userManager.CreateAsync(poweruser, userPassword);
                await userManager.AddToRoleAsync(poweruser, "Admin");
            }


            // tehdään myös toimittaja dummy account
            var manageruser = new AppUser
            {
                UserName = Configuration.GetSection("Manager")["Email"],
                Email = Configuration.GetSection("Manager")["Email"],
            };

            string managerPassword = Configuration.GetSection("Manager")["Password"];

            var mUser = await userManager.FindByEmailAsync("manager@webharkka.fi");

            if (mUser == null)
            {
                //await userManager.DeleteAsync(mUser);
                var createManagerUser = await userManager.CreateAsync(manageruser, managerPassword);
                await userManager.AddToRoleAsync(manageruser, "Manager");
            }

            var customeruser = new AppUser()
            {
                UserName = "test@webharkka.fi",
                Email = "test@webharkka.fi"
            };

            var cUser = await userManager.FindByEmailAsync("test@webharkka.fi");

            if (cUser == null)
            {
                var createManagerUser = await userManager.CreateAsync(customeruser, "Test12#");
                await userManager.AddToRoleAsync(customeruser, "Customer");

                await CreateRandomWorkOrders(customeruser, dbContext);
            }
            
        }

        private static async Task CreateRandomWorkOrders(AppUser user, ApplicationDbContext dbContext)
        {
            for (int i = 0; i <= 8; i++)
            {
                WorkOrder order = new WorkOrder()
                {
                    User = user,
                    Description = GetDescription(),
                    OrderDate = GetDateTime(),
                    State = OrderState.Ordered
                };

                await dbContext.AddAsync(order);
            }
        }

        private static DateTime GetDateTime()
        {
            Random rnd = new Random();
            DateTime start = new DateTime(2016, 12, 12);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(rnd.Next(range));
        }

        public static string GetDescription()
        {
            Random rnd = new Random();

            string[] words = { "siivotkaa", "piha", "kiinteistö", "asunto", "talli", "korjatkaa", "pieni", "iso", "talo", "likainen", "tuhotkaa", "muurahaispesä" };

            string result = "";
            for (int i = 0; i <= 10; i++)
            {
                result += $"{words[rnd.Next(0, words.Length)]} ";
            }
            
            return result;
        }
    }
}
