﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebHarkka.Models;

namespace WebHarkka.Data
{
    public class ApplicationDbContext : IdentityDbContext<AppUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<AppUser>(usr =>
            {

                usr.HasMany(u => u.WorkRequests) // voi olla monta työtarjousta
                    .WithOne(wr => wr.User) // yhdellä käyttäjällä
                    .HasForeignKey(wr => wr.UserId) // Foreign key
                    .OnDelete(DeleteBehavior.Cascade); // kun käyttäjä poistetaan, poistetaan myös tämän työtarjoukset

                usr.HasMany(u => u.WorkOrders)
                    .WithOne(wo => wo.User)
                    .HasForeignKey(wo => wo.UserId)
                    .OnDelete(DeleteBehavior.Cascade);


                usr.HasOne(u => u.Property)
                    .WithOne(p => p.Owner)
                    .HasForeignKey<Property>(p => p.OwnerId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
            
            builder.Entity<Property>(pr =>
            {
                pr.HasOne(p => p.PropertyType);
            });
            
            //builder.Entity<WorkOrder>(order =>
            //{
            //    order.HasMany(o => o.Comments)
            //        .WithOne();

            //    order.HasMany(o => o.Materials)
            //        .WithOne();

            //    order.HasOne(o => o.WorkRequest)
            //        .WithOne(r => r.WorkOrder)
            //        .HasForeignKey<WorkRequest>(r => r.WorkOrderId);
            //});

            //builder.Entity<WorkRequest>(request =>
            //{
            //    request.HasOne(r => r.WorkOrder)
            //        .WithOne(o => o.WorkRequest)
            //        .HasForeignKey<WorkOrder>(o => o.WorkRequestId);
            //});
            
        }

        public DbSet<WebHarkka.Models.WorkRequest> WorkRequests { get; set; }
        public DbSet<WebHarkka.Models.WorkOrder> WorkOrders { get; set; }
        public DbSet<WebHarkka.Models.Comment> Comments { get; set; }
        public DbSet<WebHarkka.Models.Material> Materials { get; set; }
        public DbSet<WebHarkka.Models.Property> Properties { get; set; }
        public DbSet<WebHarkka.Models.PropertyType> PropertyTypes { get; set; }

    }
}
