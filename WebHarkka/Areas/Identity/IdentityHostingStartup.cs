﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebHarkka.Data;
using WebHarkka.Models;

[assembly: HostingStartup(typeof(WebHarkka.Areas.Identity.IdentityHostingStartup))]
namespace WebHarkka.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {

                // Täällä suoritetaan identity init kuten roolien luonnit

            });
        }
    }
}