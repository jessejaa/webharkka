﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebHarkka.Models
{
    public class WorkRequest
    {
        public int Id { get; set; }

        // User foreign key
        public string UserId { get; set; }
        [DisplayName("Tarjoaja")]
        public AppUser User { get; set; }
        
        [DisplayName("Tarjouksen kuvaus")]
        public string Description { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Tarjottu")]
        public DateTime RequestDate { get; set; }

        [DisplayName("Tila")]
        public RequestState State { get; set; }
        
        [DisplayName("Kustannusarvio")]
        public double CostEstimate { get; set; }
    }

    public enum RequestState
    {
        Requested,
        Responded,
        Accepted,
        Declined
    }
}
