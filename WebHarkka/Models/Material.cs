﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebHarkka.Models
{
    public class Material
    {
        public int Id { get; set; }

        [DisplayName("Materiaali")]
        public string Name { get; set; } // Esim. käytetyt työtunnit

        [DisplayName("Kuvaus")]
        public string Description { get; set; }

        [DisplayName("Määrä")]
        public int Quantity { get; set; } // Esim. 12
    }
}
