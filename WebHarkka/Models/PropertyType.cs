﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebHarkka.Models
{
    public class PropertyType
    {
        public int Id { get; set; }

        [DisplayName("Nimi")]
        public string Name { get; set; }
    }
}
