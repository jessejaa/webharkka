﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebHarkka.Models
{
    public class WorkOrder
    {
        public int Id { get; set; }

        // User foreign key
        public string UserId { get; set; }

        [DisplayName("Tilaaja")]
        public AppUser User { get; set; }

        [DisplayName("Työn kuvaus")]
        public string Description { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Tilattu")]
        public DateTime OrderDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Aloitettu")]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Valmistunut")]
        public DateTime? DoneDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Hyväksytty")]
        public DateTime? ApproveDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Hylätty")]
        public DateTime? RejectDate { get; set; }

        [DisplayName("Tila")]
        public OrderState State { get; set; }

        // Tarjoajan kommentit
        [DisplayName("Kommentit")]
        public List<Comment> Comments { get; set; }

        // Toimittajan käytetyt resurssit
        [DisplayName("Käytetyt resurssit")]
        public List<Material> Materials { get; set; }
    }

    public enum OrderState
    {
        Ordered,
        Started,
        Completed,
        Accepted,
        Declined
    }
}
