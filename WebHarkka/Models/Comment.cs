﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebHarkka.Models
{
    public class Comment
    {
        public int Id { get; set; }

        [DisplayName("Kommentti")]
        public string Text { get; set; }
    }
}
