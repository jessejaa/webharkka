﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebHarkka.Models
{
    public class AppUser : IdentityUser
    {
        [DisplayName("Nimi")]
        public string Name { get; set; }

        [DisplayName("Osoite")]
        public string Address { get; set; }

        [DisplayName("Laskutusosoite")]
        public string BillingAddress { get; set; }

        // public string PhoneNumber { get; set; }  -- Määritelty jo IdentityUser -luokassa

        // public string Email { get; set; }        -- Määritelty jo IdentityUser -luokassa

        [DisplayName("Kiinteistö")]
        public Property Property { get; set; }

        [DisplayName("Työtarjoukset")]
        public ICollection<WorkRequest> WorkRequests { get; set; }

        [DisplayName("Työtilaukset")]
        public ICollection<WorkOrder> WorkOrders { get; set; }
    }
}
