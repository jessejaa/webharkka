﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebHarkka.Models
{
    public class Property
    {
        public int Id { get; set; }

        public string OwnerId { get; set; }

        [DisplayName("Omistaja")]
        public AppUser Owner { get; set; }

        [DisplayName("Kiinteistön tyyppi")]
        public PropertyType PropertyType { get; set; }

        [DisplayName("Kiinteistön pa")]
        public decimal PropertyArea { get; set; }

        [DisplayName("Tontin pa")]
        public decimal LotArea { get; set; }
    }
}
