﻿$(function () {
    updateRequestsTable("", "01-01-2012", -1);










    var modalContainer = $('#modal-placeholder');

    Date.prototype.toDateInputValue = (function () {
        console.log("toDateInput");

        var local = new Date(this);
        local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
        return local.toJSON().slice(0, 10);
    });

    $("#search-date").val(new Date().toDateInputValue());
    
    $('button[data-toggle="modal"]').click(function (event) {
        var url = $(this).data('url');

        $.get(url).done(function (data) {
            modalContainer.html(data);
            modalContainer.find('.modal').modal('show');
        });

    });

    modalContainer.on('click', '[data-save="modal"]', function (event) {
        event.preventDefault();
        console.log("POST");

        var form = modalContainer.find('.modal').find('form');
        var url = form.attr('action');
        var data = form.serialize();

        console.log(data);

        $.post(url, data).done(function (data) {
            modalContainer.find('.modal').modal('hide');
            
            updateRequestsTable("", "01-01-2012", -1);

        });
    });


    var tableContainer = $('#table-placeholder');
    var searchButton = $('#search-button');
    var searchText = $('#search-text');

    searchButton.click(function (event) {
        console.log("test");

        var url = $(this).data('url');

        var query = $('#search-text').val();
        var date = $('#search-date').val();
        var state = $('#search-state').val();

        $.ajax({
            url: url,
            type: "get",
            data: {
                search: query,
                date: date,
                state: state
            },
            success: function (response) {
                $('#table-placeholder').html(response);
            }
        });
    });

    
    function updateRequestsTable(query, date, state) {
        console.log("updateRequestsTable()");

        var url = "WorkRequests/RequestsTable";

        $.ajax({
            url: url,
            type: "get",
            data: {
                search: query,
                date: date,
                state: state
            },
            success: function (response) {
                $('#table-placeholder').html(response);
            }
        });
    }
});