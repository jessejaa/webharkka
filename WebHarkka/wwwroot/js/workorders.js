﻿
$(function () {
    // Kun sivu ladataan ladataan ajaxilla taulu tilauksista
    updateOrdersTable("", "01-01-2012", -1);

    Date.prototype.toDateInputValue = (function () {
        var local = new Date(this);
        local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
        return local.toJSON().slice(0, 10);
    });

    $("#search-date").val(new Date().toDateInputValue());

    var modalContainer = $('#modal-placeholder');

    $('button[data-toggle="modal"]').click(function (event) {

        console.log("GET");

        var url = $(this).data('url');

        $.get(url).done(function (data) {
            modalContainer.html(data);
            modalContainer.find('.modal').modal('show');
        });

    });

    modalContainer.on('click', '[data-save="modal"]', function (event) {
        event.preventDefault();
        console.log("POST");

        var form = modalContainer.find('.modal').find('form');
        var url = form.attr('action');
        var data = form.serialize();

        $.post(url, data).done(function (data) {
            modalContainer.find('.modal').modal('hide');
        });
    });


    var tableContainer = $('#table-placeholder');
    var searchButton = $('#search-button');
    var searchText = $('#search-text');

    searchButton.click(function (event) {
        console.log("search button");
        var url = $(this).data('url');

        var query = $('#search-text').val();
        var date = $('#search-date').val();
        var state = $('#search-state').val();

        $.ajax({
            url: url,
            type: "get",
            data: {
                search: query,
                date: date,
                state: state
            },
            success: function (response) {
                $('#table-placeholder').html(response);
            }
        });
    });

    function updateOrdersTable(query, date, state) {
        console.log("updateOrdersTable()");

        var url = "WorkOrders/OrdersTable";

        $.ajax({
            url: url,
            type: "get",
            data: {
                search: query,
                date: date,
                state: state
            },
            success: function (response) {
                $('#table-placeholder').html(response);
            }
        });
    }

    function updateRequestsTable(query, date, state) {
        console.log("updateRequestsTable()");

        var url = "WorkRequests/RequestsTable";

        $.ajax({
            url: url,
            type: "get",
            data: {
                search: query,
                date: date,
                state: state
            },
            success: function (response) {
                $('#table-placeholder').html(response);
            }
        });
    }

});