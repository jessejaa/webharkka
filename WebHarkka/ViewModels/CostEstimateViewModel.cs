﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHarkka.ViewModels
{
    public class CostEstimateViewModel
    {
        public int RequestId { get; set; }
        public int CostEstimate { get; set; }
    }
}
