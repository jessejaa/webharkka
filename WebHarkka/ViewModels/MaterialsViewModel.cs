﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebHarkka.Models;

namespace WebHarkka.ViewModels
{
    public class MaterialsViewModel
    {
        public List<Material> Materials { get; set; }
        public Material NewMaterial { get; set; }
    }
}
